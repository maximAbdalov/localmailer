<?php

namespace Foxtes\LocalMailer;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class LocalMailerServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/config/local-mailer.php';
        $this->mergeConfigFrom($configPath, 'local-mailer');

        $this->publishes([
            // Config
            $configPath => config_path('local-mailer.php'),
        ], 'local-mailer');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureMailer();
    }

    /**
     * Set laravel mail config from package config file if env non-production
     */
    private function configureMailer()
    {
        if (!App::environment('production')) {
            $localEmailConf = config('local-mailer.to');
            if ($localEmailConf && is_array($localEmailConf)) {
                Config::set('mail.to', $localEmailConf);
            }
        }
    }
}
