<?php

return [

    /**
     * Email for send all emails if environment is non-production.
     */

    'to' => [
        'address' => 'fish-mirrow@yandex.ru',
        'name' => 'Non-production Email!'
    ]
];
