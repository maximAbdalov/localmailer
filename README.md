## Laravel non-production email recipient

### Installation
Install this package with composer:
```
composer require foxtes/local-mailer
```
Copy the config files
```
php artisan vendor:publish --tag="local-mailer"
```

You can change email in publishing config file.